#!/bin/bash

# script to install a bunch of Anaconda python libraries onto
# a generic Ubuntu VM

sudo apt-get update
sudo apt-get dist-upgrade -y
sudo apt-get autoremove -y

sudo apt-get install -y wget bzip2 ca-certificates git build-essential python-dev unzip libav-tools 
sudo apt-get update 
sudo apt-get install -y --no-install-recommends libav-tools 
	
# Configure environment
CONDA_DIR=/opt/conda
PATH=$CONDA_DIR/bin:$PATH
SHELL=/bin/bash
NB_USER=bitnami
NB_UID=1000
HOME=/home/$NB_USER
LC_ALL=en_US.UTF-8
LANG=en_US.UTF-8
LANGUAGE=n_US.UTF-8


# Install conda as bitnami
cd /tmp && \
    sudo mkdir -p $CONDA_DIR && \
    wget --quiet https://repo.continuum.io/miniconda/Miniconda3-4.1.11-Linux-x86_64.sh && \
    echo "efd6a9362fc6b4085f599a881d20e57de628da8c1a898c08ec82874f3bad41bf *Miniconda3-4.1.11-Linux-x86_64.sh" | sudo sha256sum -c - && \
    sudo /bin/bash Miniconda3-4.1.11-Linux-x86_64.sh -f -b -p $CONDA_DIR && \
    sudo rm Miniconda3-4.1.11-Linux-x86_64.sh && \
    sudo $CONDA_DIR/bin/conda install --quiet --yes conda==4.1.11 && \
    sudo $CONDA_DIR/bin/conda config --system --add channels conda-forge && \
    sudo $CONDA_DIR/bin/conda config --system --set auto_update_conda false 

# Install Jupyter notebook as jovyan
sudo $CONDA_DIR/bin/conda install --yes \
    'notebook=4.2*' \
    jupyterhub=0.7 
    

#----------- scipy

# Install Python 3 packages
sudo $CONDA_DIR/bin/conda install --yes \
    'nomkl' \
    'ipywidgets=5.2*' \
    'pandas=0.19*' \
    'numexpr=2.6*' \
    'matplotlib=1.5*' \
    'scipy=0.17*' \
    'seaborn=0.7*' \
    'scikit-learn=0.17*' \
    'scikit-image=0.11*' \
    'sympy=1.0*' \
    'cython=0.23*' \
    'patsy=0.4*' \
    'statsmodels=0.6*' \
    'cloudpickle=0.1*' \
    'dill=0.2*' \
    'numba=0.23*' \
    'bokeh=0.11*' \
    'sqlalchemy=1.0*' \
    'hdf5=1.8.17' \
    'h5py=2.6*' \
    'vincent=0.4.*' \
    'beautifulsoup4=4.5.*' \
    'openpyxl' \
    'pandas-datareader' \
    'ipython-sql' \
    'pandasql' \
    'memory_profiler'\
    'psutil' \
    'cythongsl' \
    'joblib' \
    'ipyparallel' \
    'xlrd' 

sudo $CONDA_DIR/bin/conda install --yes 'conda-build'
sudo $CONDA_DIR/bin/conda clean -tipsy


# Install Python 2 packages
sudo $CONDA_DIR/bin/conda create --yes -p $CONDA_DIR/envs/python2 python=2.7 \
    'nomkl' \
    'ipython=4.2*' \
    'ipywidgets=5.2*' \
    'pandas=0.19*' \
    'numexpr=2.6*' \
    'matplotlib=1.5*' \
    'scipy=0.17*' \
    'seaborn=0.7*' \
    'scikit-learn=0.17*' \
    'scikit-image=0.11*' \
    'sympy=1.0*' \
    'cython=0.23*' \
    'patsy=0.4*' \
    'statsmodels=0.6*' \
    'cloudpickle=0.1*' \
    'dill=0.2*' \
    'numba=0.23*' \
    'bokeh=0.11*' \
    'hdf5=1.8.17' \
    'h5py=2.6*' \
    'sqlalchemy=1.0*' \
    'pyzmq' \
    'vincent=0.4.*' \
    'beautifulsoup4=4.5.*' \
    'xlrd'

sudo $CONDA_DIR/bin/conda clean -tipsy

# Add shortcuts to distinguish pip for python2 and python3 envs
sudo ln -s $CONDA_DIR/envs/python2/bin/pip $CONDA_DIR/bin/pip2 
sudo ln -s $CONDA_DIR/bin/pip $CONDA_DIR/bin/pip3

sudo $CONDA_DIR/bin/conda install --yes \
    'numpy=1.11*' \
    'pillow=3.4*' \
    'requests=2.12*' \
    'nose=1.3*' \
    'pystan=2.8*' 

sudo $CONDA_DIR/bin/conda clean -tipsy


# we need dvipng so that matplotlib can do LaTeX
# we want OpenBLAS for faster linear algebra as described here: http://brettklamer.com/diversions/statistical/faster-blas-in-r/
sudo apt-get update 
sudo apt-get install -yq --no-install-recommends dvipng libopenblas-base  liblapack3 libblas-dev liblapack-dev 
 
# ggplot
#
sudo $CONDA_DIR/bin/pip install --upgrade pip
sudo -H $CONDA_DIR/bin/pip install ggplot

sudo $CONDA_DIR/bin/conda clean -tipsy

#
# activate anaconda python via the .profile for the user
#
echo " " >> $HOME/.profile
echo "# activate anaconda python 2 " >> $HOME/.profile
echo "if [ -f /opt/conda/envs/python2/bin/activate ] ; then" >> $HOME/.profile
echo "    source /opt/conda/envs/python2/bin/activate " /opt/conda/envs/python2 >> $HOME/.profile
echo "fi" >> $HOME/.profile
echo " " >> $HOME/.profile
 

#
# some additional editors
#
sudo apt-get update
sudo apt-get install -y emacs xemacs21 gedit


